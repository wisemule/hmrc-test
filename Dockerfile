FROM openjdk:8-jre-alpine
RUN apk --no-cache add curl
COPY target/hmrc-test-1.0-SNAPSHOT.jar /app/hmrc-test-1.0-SNAPSHOT.jar
COPY src/test/resources/samples /app/samples/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/hmrc-test-1.0-SNAPSHOT.jar"]
HEALTHCHECK CMD wget --quiet --tries=1 --spider	http://localhost:8080/actuator/health || exit 1
EXPOSE 8080
