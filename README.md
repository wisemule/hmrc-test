HMRC Test - REST API Demo
=========================

### To build this project use

```
    mvn clean package
```

### Tests

```
    mvn test
```
 
### To run this project from within Maven

```
    mvn spring-boot:run
```

### Docker commands

```
     docker build . 
     docker run -d -p 8080:8080 <docker-image-id-from-above-command>
```

### Testing via Curl

#### Curl inside Docker

 * Copy container id from above command, and start interactive session as below: 

```
    docker exec -it <container-id> sh

```
 
 * Curl commands:
 
```
    curl -d "@app/samples/customer.json" -H "content-type:application/json" http://localhost:8080/v1/customer -v 
    curl -d "@app/samples/invalidCustomer.json" -H "content-type:application/json" http://localhost:8080/v1/customer -v

```
    
#### Application Health/Info
 * Info: http://localhost:8080/actuator/info
 * Health: Camel Routes http://localhost:8080/actuator/health
 * Camel Routes: http://localhost:8080/actuator/camelroutes

##### API Doc / Swagger
 * http://localhost:8080/v1/api-doc/swagger.json for JSON
 * http://localhost:8080/v1/api-doc/swagger.yaml for YAML
