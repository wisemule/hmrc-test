package uk.co.wiselabs.models;

import lombok.Data;

@Data
public class Customer {

    private String id;
    private String firstName;
    private String lastName;
    private String address;

}
