package uk.co.wiselabs.routes;

import org.apache.camel.Exchange;
import org.apache.camel.ValidationException;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class CustomerRoute extends RouteBuilder {

    public void configure() throws Exception {

        onException(ValidationException.class)
                .log("${exception.message}")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(400))
                .handled(true)
                .setBody(simple("{\"message\": \"${exception.message}\"}"));

        onException(Exception.class)
                .log("Server processing error")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
                .handled(true)
                .setBody(simple("{\"message\": \"server error\"}"));

        restConfiguration()
                .component("servlet")
                .bindingMode(RestBindingMode.auto)
                .dataFormatProperty("prettyPrint", "true")
                .apiContextPath("/api-doc");

        rest().consumes("application/json").produces("application/xml").post("/customer")
                .to("log:DEBUG?showBody=true&showHeaders=true")
                .to("direct:frontendvalidation");

        // frontend validation route
        from("direct:frontendvalidation")
                .to("json-validator:schemas/customerSchema.json")
                .log("frontend validated, sending to transform.")
                .to("direct:transform");

        // transformation route
        from("direct:transform").
                to("xj:customerJson2xml.xsl?transformDirection=JSON2XML")
                .log("body ${body}")
                .to("direct:backendvalidation");

        // backend validation route
        from("direct:backendvalidation")
                .to("validator:schemas/customerSchema.xsd")
                .log("backend validated.")
                // create the resource in backend
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201));
    }
}
