package uk.co.wiselabs.routes;

import org.apache.camel.builder.RouteBuilder;

public class MockReplyRoute extends RouteBuilder {

    @Override
    public void configure() {
        from("direct:in")
                .inOut("mock:replying")
                .to("mock:out");
    }
}