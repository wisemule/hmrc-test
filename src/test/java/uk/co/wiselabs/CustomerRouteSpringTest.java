package uk.co.wiselabs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.wiselabs.config.CustomerApplication;
import uk.co.wiselabs.models.Customer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerRouteSpringTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void happyPathTest() throws IOException {
        Map<String, Object> testData = new LinkedHashMap<>();
        testData.put("id", "d290f1ee-6c54-4b01-90e6-d701748f0851");
        testData.put("firstName", "Mujahed");
        testData.put("lastName", "Syed");
        testData.put("address", "Surrey, U.K");

        // Call the REST API
        ResponseEntity<String> response = restTemplate.postForEntity("/v1/customer", testData, String.class);

        ObjectMapper objectMapper = new XmlMapper();
        InputStream inputStream = new ByteArrayInputStream(response.getBody().getBytes(StandardCharsets.UTF_8));
        Customer customer =objectMapper.readValue(inputStream, Customer.class);

        assertThat(response.getStatusCode(), is(equalTo(HttpStatus.CREATED)));
        assertThat(customer.getId(), is("d290f1ee-6c54-4b01-90e6-d701748f0851"));
        assertTrue(customer.getFirstName().equals("Mujahed"));
        assertTrue(customer.getAddress().equals("Surrey, U.K"));
    }

    @Test
    public void failedValidationTest() throws IOException {
        Map<String, Object> testData = new LinkedHashMap<>();
        testData.put("id", "d290f1ee-6c54-4b01-90e6-d701748f0851");
        testData.put("firstName", "Mujahed");
        testData.put("lastName", "Syed");

        // Call the REST API
        ResponseEntity<String> response = restTemplate.postForEntity("/v1/customer", testData, String.class);

        assertThat(response.getStatusCode(), is(equalTo(HttpStatus.BAD_REQUEST)));
        assertThat(response.getBody(), containsString("JSon validation error with 1 errors. Exchange[]"));
    }

}
