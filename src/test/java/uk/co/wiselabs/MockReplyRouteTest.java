package uk.co.wiselabs;

import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.builder.SimpleBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import uk.co.wiselabs.routes.MockReplyRoute;

public class MockReplyRouteTest extends CamelTestSupport {

    @EndpointInject(value = "mock:replying")
    private MockEndpoint mockReplying;

    @EndpointInject(value = "mock:out")
    private MockEndpoint mockOut;

    @Produce(value = "direct:in")
    ProducerTemplate in;

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new MockReplyRoute();
    }

    @Test
    public void testReplyingFromMockByExpression() throws InterruptedException {
        mockReplying.returnReplyBody(SimpleBuilder.simple("Hello ${body}"));

        mockReplying.expectedBodiesReceived("Camel");
        mockOut.expectedBodiesReceived("Hello Camel");

        in.sendBody("Camel");

        assertMockEndpointsSatisfied();
    }

}
